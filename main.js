import Exponent from 'exponent';
import React, { Component } from 'react';
import state from './src/state'
import {
  AppRegistry,
} from 'react-native';

import { observer} from 'mobx-react/native';
import {useStrict} from 'mobx'

import CoverFlow from './src/CoverFlow';

//useStrict(true);


@observer
export default class ExampleList extends Component {

  componentWillMount() {
    state.loadState()
    cacheImages([
      require('./assets/back-button.android.png'),
      require('./assets/back-button.ios.png'),
    ]);
  }

  render() {
    return state.isLoaded ? <CoverFlow /> : null
  }
}

function cacheImages(images) {
  return images.map(image => Exponent.Asset.fromModule(image).downloadAsync());
}

Exponent.registerRootComponent(ExampleList);
