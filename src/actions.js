import state from './state'
import { observable, computed, action } from 'mobx';
const ReactNative = require('react-native');

const {
  NavigationExperimental,
} = ReactNative;

const {
  CardStack: NavigationCardStack,
  StateUtils: NavigationStateUtils,
} = NavigationExperimental;


class Actions {

  @action presentTodoModal(index) {
    console.log('TodoModalVisible')
    state.todoModalVisible = true
  }

  @action dismissTodoModal() {
    console.log("CALLED")
    state.todoModalVisible = false
  }

   @action presentListModal(index) {
    console.log("ListmodalVisibile")
    state.listModalVisible = true
  }

  @action dismissListModal() {
    state.listModalVisible = false
  }

  @action setLatestActiveTodoList(index){
    state.latestActiveTodoList = index
    console.log("LatestTodoUpdatedTo" + index.toString())
  }

  @action addTodo(list, todo) {
    var length = state.todoLists[list].todos.length
    state.todoLists[list].todos.push({index: length, body: todo, done: false, imageURL: ""})
    console.log(state.todoLists[list].todos)
  } 

  @action toggleTodo(list, index) {
    var todo = state.todoLists[list].todos[index]
    console.log(index)
    console.log(todo)
    state.todoLists[list].todos[index] = {...todo, done: !todo.done}
  }

  @action addList(name, location) {
    state.todoLists.push({
            name: name,
            location: location,
            todos: []
    })
  }

  @action deleteList(index) {
    state.todoLists.splice(index, 1)
  }

}

module.exports = new Actions()