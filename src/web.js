'use strict';


var searchCX = "011158106429585852427:koppolnq4u0"
var searchAPIKey = "AIzaSyDBAPbrlI5BKqHBBKUU1XFVgr_STtL1T2U"

var placesAPIKey = 'AIzaSyC62n_U0KwKpMGYwkyyM6Ni9aDEzeoKEJo'

var darkSkyAPIKey = '3f419230d1e6c721a7baa8f8e2b19851'



var search = (query) => {
    var url = "https://www.googleapis.com/customsearch/v1?key=" + searchAPIKey + "&cx=" + searchCX + "&q=" + query +  "&searchType=image"
    return fetch(url).then((response) => response.json())
}


var placeAutocomplete = (query) => {
    var url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + query + '&types=(cities)&location=0,0&radius=20000000&key=' + placesAPIKey
    return fetch(url).then((response) => response.json())
}

var placeCoords = (placeID) => {
    var url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='+ placeID +'&key=' + placesAPIKey
    return fetch(url).then(response => response.json()).then(res => res.result.geometry.location)
}


var weather = (lat,lng) => {
    var url = 'https://api.darksky.net/forecast/' + darkSkyAPIKey + '/' + lat + ',' + lng 
    return fetch(url).then(response => response.json())
}


module.exports = {search, placeAutocomplete, placeCoords, weather} 
