import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView
} from 'react-native';
import Dimensions from 'Dimensions';

var windowWidth = Dimensions.get('window').width;



class PagingView extends Component {
  constructor() {
    super();
    this.state = {
      index: 0
    };
  }

  pageIndicator(pages, index) {
    index = index ? index > 0 : 0
    index = index ? index < pages -1 : pages -1
    dots = []
    console.log(index)
    for (var i =0; i < pages; i++){
      if (i == index ) {
        dots.unshift(<View key = {i}style = {[styles.dot, { backgroundColor: 'white'}]}/>)
      } else { 
        dots.unshift(<View key = {i} style = {styles.dot}/>)
      }
    }
    return(
      <View style = {styles.dotContainer} >
          {dots}
        </View>
    )
  }

  handleScroll = (event) => {
    console.log(event.nativeEvent.contentOffset.x)
    index = Math.floor(event.nativeEvent.contentOffset.x/windowWidth)
    this.setState({index: index})
  }
  

  render() {
    return (
      <View style = {{ height: 200}}>
        {this.pageIndicator(this.props.pages, this.state.index)}
        <ScrollView horizontal = {true} pagingEnabled = {true} showsHorizontalScrollIndicator = {false} scrollEventThrottle = {50} onScroll = {this.handleScroll}>
            {this.props.children}
        </ScrollView>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  dot: {
    backgroundColor: 'rgba(255,255,255,.5)',
    height: 6,
    width: 6,
    borderRadius: 3,
    zIndex: 1,
    marginLeft: 1.5,
    marginRight: 1.5
  },
  dotContainer: {
    height: 10,
    position: 'absolute',
    bottom: 5,
    left: 0,
    right: 0,
    zIndex: 1,
    justifyContent: 'center',
    flexDirection: 'row'
  }
})

module.exports = PagingView
