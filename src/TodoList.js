import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ListView,
  TouchableHighlight,
  Modal,
  Animated,
  ScrollView
} from 'react-native';
import state from './state'

import { observer } from 'mobx-react/native';

import actions from './actions'
import {search} from "./web"

import {
  Button,
  FormInput,
  CheckBox
} from 'react-native-elements'

import {
  Components
} from 'exponent';

import PagingView from './PagingView'
import Dimensions from 'Dimensions';

var windowWidth = Dimensions.get('window').width;

dates = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']


@observer
class TodoList extends Component {
  constructor() {
    super();
    this.state = {
      modalVisible: false,
      todoInput: "",
      imageOpacity: new Animated.Value(0),
      imageURL: ''
    };
  }
  
  componentWillMount() {
    this.getImageURL()
  }

  getImageURL() {
    search(state.todoLists[this.props.index].location).then(json => {
      this.setState({imageURL: json.items[0].link})
      state.todoLists[this.props.index].imageURL = json.items[0].link
    })
  }


  onImageLoad = () => {
    Animated.timing(this.state.imageOpacity, {
      toValue: 1,
      duration: 250
    }).start()
  }

  renderRow = (rowData) => {
    return (
      <CheckBox disabled = {this.props.disabled} checked={rowData.done} title = {rowData.body} onPress = {() => {actions.toggleTodo(this.props.index, rowData.index)}}/>
    )
  }


  renderModal() {
    return (
      <Modal
            animationType={"fade"}
            transparent={true}
            visible={state.todoModalVisible}
            >
          <View style = {styles.modalBackground}>
            <View style = {styles.modal}>
              <TouchableHighlight underlayColor = 'rgba(0,0,0,0)' style = {[styles.buttonContainer, styles.xButtonContainer]} onPress={() => {console.log("SOMETHING");actions.dismissTodoModal()}}>
                <Image source = {require('../assets/x.png')} style = {[styles.button, styles.xButton]}>
                </Image>
              </TouchableHighlight>
              <Text style = {styles.modalTitle}>New Todo</Text>
              <FormInput autoFocus = {true} placeholder = "Add an an item for your trip" onChangeText = {(val) => this.setState({todoInput: val})}/>
              <View style = {{height: 30}} />
              <Button
                raised
                title='Create' 
                backgroundColor = '#272727'
                onPress = {() => {
                  this.setState({modalVisible: false})
                  actions.dismissTodoModal()
                  actions.addTodo(state.latestActiveTodoList, this.state.todoInput)
                  }}/>
            </View>
            <View style = {{height: 100}}/>
          </View>
        </Modal>
    )
  }

  renderForecast() {

    var d = new Date();
    var n = d.getDay();
    var days = []
    for (var i =0; i < 7; i++) {
      days.push(<View key = {i} style = {{margin: 1, alignItems: 'center'}}>
        <Text style = {styles.weatherText}>{dates[(n+i)%7]}</Text>
        <Image style = {styles.weatherImage} source = {require('../assets/partlyCloudy.png')}/>
        <Text style = {styles.temperatureText}>70°/60°</Text>
        </View>
      )
    } 

    return days
  }

  render() {
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => JSON.stringify(r1) !== JSON.stringify(r2)})
    var dataSource = ds.cloneWithRows(state.todoLists[this.props.index].todos.slice())
    var data = state.todoLists[this.props.index]
    return (
      <View style = {{flex: 1}}>
        {this.renderModal()}
        <Animated.Image 
                  style = {[{opacity: this.state.imageOpacity, backgroundColor: '#272727'}, styles.image]} onLoad = {this.onImageLoad}
                  source={data.imageURL != "" ? {uri: data.imageURL}: null}/>
        <Components.BlurView tintEffect="dark" style={[{opacity: .2}, styles.image]}/>
        <PagingView pages = {2}>
          <View style = {styles.page}>
            <Text style = {styles.title}>{data.name}</Text>
            <Text style = {styles.subTitle}>{data.location}</Text>
          </View>
          <View style = {styles.page}>
            <View style = {{height: 100, width: windowWidth, flexDirection: 'row', marginTop: 60, justifyContent: 'center'}}>
            {this.renderForecast()}
            </View>
          </View>
        </PagingView>
        <ListView
          enableEmptySections={true}
          dataSource={dataSource}
          renderRow={this.renderRow}
        />
      </View>
    );
  }
}

var styles = StyleSheet.create({
  wrapper: {
    height: 200,
  },
  text: {
    margin: 10,
    color: '#4B5159',
    fontSize: 14,
  },
  title: {
    marginLeft: 20,
    marginBottom: 5,
    margin: 10,
    marginTop: 60,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
    backgroundColor: 'rgba(0,0,0,0)',
  },
  subTitle: {
    marginLeft: 20,
    color: 'white',
    fontWeight: '200',
    fontSize: 18,
    backgroundColor: 'rgba(0,0,0,0)',
  },
  listItem: {
    justifyContent: 'center',
    height: 60,
    borderBottomColor: '#EDEFF2',
    borderBottomWidth: 1,
  },
  page: {
    width: windowWidth,
    height: 200,
    //backgroundColor: '#4B5159',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  image: {
    position: 'absolute',
    height: 200,
    width: windowWidth,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  buttonContainer: {
    height: 40,
    width: 40,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    width: 35,
    height: 35,
    tintColor: 'white',
    resizeMode: 'contain'
  },
  xButtonContainer: {
    position: 'absolute',
    marginTop: 5,
  },
  xButton: {
    tintColor: '#74838F',
  },
  modal: {
    margin: 30,
    height: 210,
    backgroundColor: 'white',
  },
  modalBackground: {
    flex: 1, 
    justifyContent: 'center',
    backgroundColor: 'rgba(52,52,52,.2)'
  },
  modalTitle: {
    fontWeight: '200',
    alignSelf: 'center',
    fontSize: 22,
    margin: 15,
    color: "#2A2A2A"
  },
  weatherText: {
    color: 'white',
    fontWeight: '200',
    fontSize: 11.5,
  },
  temperatureText: {
    color: 'white',
    fontWeight: '400',
    fontSize: 11.5,
  },
  weatherContainer: {
    height: 100,
    marginTop: 60,
    flex: 1,
    flexDirection: 'row',
  },
  weatherImage: {
    resizeMode: 'contain',
    width: windowWidth/7 -5,
    height: 60,
  }
})

module.exports = TodoList
