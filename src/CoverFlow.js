/* eslint-disable import/no-commonjs */

import React, { Component } from 'react';
import { View, 
  Image, 
  Text, 
  StyleSheet, 
  Animated, 
  TouchableOpacity, 
  TouchableHighlight, 
  Modal, 
  ListView,
  LayoutAnimation 
} from 'react-native';
import { TabViewAnimated, TabViewPage } from 'react-native-tab-view';
import Dimensions from 'Dimensions';
import TodoList from './TodoList'
import state from './state'
import NewListModal from './NewListModal'

import { observer } from 'mobx-react/native';
import actions from './actions'

import {placeAutocomplte} from './web'

import {
  Button,
  FormInput
} from 'react-native-elements'

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;
var shrinkFactor = .7

@observer
export default class CoverFlow extends Component {

    constructor() {
    super()

    this.state = {
      index: state.todoLists.map.length > 1 ? 1 : 0,
      routes: state.todoLists.map((x, key) => ({ key: key.toString(), keyInt: key })),
      scaling: shrinkFactor,
      newListModalVisible: false,
      newTodoModalVisibile: false,
      deleteListMode: false
    };
  }

  buildCoverFlowStyle = ({ layout, position, route, navigationState }) => {
    const { width } = layout;
    const { routes } = navigationState;
    const currentIndex = routes.indexOf(route);
    var inputRange = routes.map((x, i) => i);
    if (inputRange.length == 0) {
      inputRange.push(0)
    }
    if (inputRange.length == 1) {
      inputRange.push(1)
    }
    const translateOutputRange = inputRange.map(i => {
      return ((screenWidth * this.state.scaling * 1) * (currentIndex - i));
    });
    const scaleOutputRange = inputRange.map(i => {
      if (currentIndex === i) {
        return 1;
      } else {
        return .9;
      }
    });
    const opacityOutputRange = inputRange.map(i => {
      if (currentIndex === i) {
        return 1;
      } else {
        return 0.3;
      }
    });

    const translateX = position.interpolate({
      inputRange,
      outputRange: translateOutputRange,
    });
    const scale = position.interpolate({
      inputRange,
      outputRange: scaleOutputRange,
    });
    const opacity = position.interpolate({
      inputRange,
      outputRange: opacityOutputRange,
    });

    return {
      width,
      transform: [
        { translateX },
        { scale },
      ],
      opacity,
    };
  };

  handleChangeTab = (index) => {
    this.setState({
      index,
    });
  };

  createList = (name, location) => {
    actions.addList(name, location)
    this.setState({
      newListModalVisible: false,
      index: state.todoLists.length - 1, 
      routes: state.todoLists.map((x, key) => ({ key: key.toString(), keyInt: key }))
    })
  }

  renderModal() {
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return (
      <NewListModal 
      createList = {this.createList} 
      visible = {this.state.newListModalVisible}
      closePressed = {() => this.setState({newListModalVisible: false})}
      />
    )
  }

  locationTextChanged = (val) => {
    this.setState({listLocation: val})
    placeAutocomplte(val).then((json) => {
      this.setState({locationPredictions: json.predictions})
    })
  }

  pageStyles = () => {
    return {
      width: screenWidth* this.state.scaling,
      height: screenHeight * this.state.scaling,
      transform: [                        
            {scale: 1},  
      ]
    }
  }

  deleteListButtonPressed = () => {
    console.log(this.state.index)
    actions.deleteList(this.state.index)
    var newIndex = state.todoLists.length > this.state.index ? this.state.index : this.state.index - 1
    this.setState({index: newIndex, routes: state.todoLists.map((x, key) => ({ key: key.toString(), keyInt: key }))})

  }

  backButtonPressed = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({scaling: shrinkFactor})
  }

  todoListPressed = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    actions.setLatestActiveTodoList(this.state.index)
    this.setState({scaling: 1, deleteListMode: false})
  }

  plusButtonPressed = () => {
    if (state.todoLists.length == 0)  {
      this.setState({newListModalVisible: true, deleteListMode: false})
    }
    if (this.state.scaling === 1) {
      actions.presentTodoModal()
    } else {
      this.setState({newListModalVisible: true, deleteListMode: false})
    }
  }

  toggleDeleteListMode= () => {
    this.setState({deleteListMode: !this.state.deleteListMode})
  }

  touchDisabled = () => this.state.scaling === 1

  renderScene = (props) => {
    return (
      
      <TabViewPage
        {...props}
        style={this.buildCoverFlowStyle(props)}
        panHandlers = {this.state.scaling === 1 ? null: TabViewPage.PanResponder.forHorizontal(props)}
        renderScene={this.renderPage}
      />
    );
  };

  renderPage = ({ route }) => {
    return (
        <View style={styles.pageContainer}>
          <TouchableOpacity onPress={this.todoListPressed} disabled = {this.touchDisabled()}>
            <View style={[styles.page, this.pageStyles()]}>
              
              <TodoList disabled = {!this.touchDisabled() }onBack = {this.backButtonPressed} index = {route.keyInt}/>
            </View>
          </TouchableOpacity>
          {this.state.deleteListMode ? <TouchableHighlight underlayColor = 'rgba(0,0,0,0)' style = {[styles.buttonContainer, styles.deleteButtonContainer]} onPress={this.deleteListButtonPressed}>
                  <Image source = {require('../assets/x.png')} style = {[styles.button, styles.deleteButton]}/>
              </TouchableHighlight>: null}
        </View>
    );
  };


  render() {
    
    return (
      <View style = {{flex: 1}}>
      <View>
      <NewListModal 
      createList = {this.createList} 
      visible = {this.state.newListModalVisible}
      closePressed = {() => this.setState({newListModalVisible: false})}
      />
      </View>
      <TouchableHighlight underlayColor = 'rgba(0,0,0,0)' style = {styles.editButtonContainer} onPress = { this.state.scaling === 1 ? this.backButtonPressed : this.toggleDeleteListMode}>
              { this.state.scaling == .7 ? <Image source = {require('../assets/x.png')} style = {styles.button} /> :
              <Image source = {require('../assets/back-button.png')} />}
      </TouchableHighlight>
      <TouchableHighlight underlayColor = 'rgba(0,0,0,0)' style = {styles.plusButtonContainer} onPress={this.plusButtonPressed}>
              <Image source = {require('../assets/plus.png')} style = {styles.button}>
              </Image>
      </TouchableHighlight>
      <TabViewAnimated
        style={[ styles.container, this.props.style ]}
        navigationState={this.state}
        renderScene={this.renderScene}
        onRequestChangeTab={this.handleChangeTab}
      />
      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#222',
  },
  pageContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  page: {
    backgroundColor: 'white',
    width: screenWidth * shrinkFactor,
    height: screenHeight * shrinkFactor,
    elevation: 12,
    shadowColor: '#000000',
    shadowOpacity: 0.5,
    shadowRadius: 8,
    overflow: 'hidden',
    shadowOffset: {
      height: 8,
    },
  },
  plusButtonContainer: {
    position: 'absolute',
    right: 0,
    zIndex: 1,
    height: 40,
    width: 40,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editButtonContainer: {
    position: 'absolute',
    left: 0,
    zIndex: 1,
    height: 40,
    width: 40,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapper: {
    height: 200,
  },
  text: {
    margin: 10,
    color: '#4B5159',
    fontSize: 14,
  },
  editText: {
    fontWeight: '400',
    alignSelf: 'center',
    fontSize: 18,
    color: 'white'
  },
  buttonContainer: {
    position: 'absolute',
    height: 40,
    width: 40,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    width: 35,
    height: 35,
    tintColor: 'white',
    resizeMode: 'contain'
  },
  deleteButtonContainer: {
    position: 'absolute',
    left: 50,
    top: 80,
    width: 30,
    height: 30,
    marginTop: 5,
    marginBottom: 0,
    zIndex: 1
  },
  deleteButton: {
    tintColor: 'white',
    borderColor: 'white',
    width: 30,
    height: 30,
    borderWidth: 1,
    borderRadius: 15,
    backgroundColor: '#c0392b'
  }
});

