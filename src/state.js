import {observable, autorun, action } from 'mobx';
const ReactNative = require('react-native');

import  {
  AsyncStorage
} from 'react-native';

var state = observable({
    listModalVisible: false,
    todoModalVisible: false,
    isLoaded: false,
    latestActiveTodoList: 1,
    todoLists: [
        {
            name: 'Meeting w/ Elon',
            location: 'San Francisco',
            todos: [
                {index: 0, body: "Learn to read", done: false}, 
                {index: 1, body: "Read a book", done: false }
            ]
        },
        {
            name: 'Meeting w/ Elon',
            location: 'San Francisco',
            todos: [
            ]
        }
    ]
})

state.saveState = () => {
    var stateToSave = { todoLists: state.todoLists.slice() }
    console.log(stateToSave)
    AsyncStorage.setItem("state", JSON.stringify(stateToSave));
}

state.loadState = () => {
    AsyncStorage.getItem("state").then(action((value) => {
        value = JSON.parse(value)
        Object.assign(state, value)
        observable(state)
        state.isLoaded = true
        state.saveState = autorun(state.saveState)
    })).done();
}


module.exports = state;