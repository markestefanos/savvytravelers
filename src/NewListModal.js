import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Modal,
  ListView,
  TouchableHighlight,
  Image,
  Text
} from 'react-native';

import {
  Button,
  FormInput
} from 'react-native-elements'

import Dimensions from 'Dimensions';
var windowWidth = Dimensions.get('window').width;



class NewListModal extends Component {
  constructor() {
    super();
    this.state = {
        listName: "",
        listLocation: "",
        locationPredictions: []
    };
  }

  createListPressed() {
    this.props.createList(this.state.listName, this.state.listLocation)
    this.setState({
        listName: "",
        listLocation: "",
        locationPredictions: []
    })
  }

  render() {
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return (
      <Modal
            animationType={"fade"}
            transparent={true}
            visible={this.props.visible} 
            >
          <View style = {styles.modalBackground}>
            <View style = {styles.modal}>
              <TouchableHighlight underlayColor = 'rgba(0,0,0,0)' style = {[styles.buttonContainer, styles.xButtonContainer]} onPress={this.props.closePressed}>
                <Image source = {require('../assets/x.png')} style = {[styles.button, styles.xButton]}>
                </Image>
              </TouchableHighlight>
              <Text style = {styles.modalTitle}>New List</Text>
              <FormInput autoFocus = {true} placeholder = "Trip Name" onChangeText = {(val) => this.setState({listName: val})}/>
              <View style = {{height: 20}}/>
              <FormInput placeholder = "Trip Location (optional)" onChangeText = {(val) => this.setState({listLocation: val})}/>
              <View style = {{height: 30}} />
              <Button
                raised
                title='Create' 
                backgroundColor = '#272727'
                onPress = {() => {
                  this.createListPressed()
                  }}/>
            </View>
            <View style = {{height: 100}}/>
          </View>
        </Modal>
    )
  }
}

var styles = StyleSheet.create({
  modal: {
    margin: 30,
    height: 270,
    backgroundColor: 'white',
  },
  modalBackground: {
    flex: 1, 
    justifyContent: 'center',
    backgroundColor: 'rgba(52,52,52,.2)'
  },
  xButtonContainer: {
    marginTop: 5,
    marginBottom: 0,
  },
  xButton: {
    tintColor: '#74838F',
  },
  buttonContainer: {
    position: 'absolute',
    height: 40,
    width: 40,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    width: 35,
    height: 35,
    tintColor: 'white',
    resizeMode: 'contain'
  },
  modalTitle: {
    fontWeight: '200',
    alignSelf: 'center',
    fontSize: 22,
    margin: 15,
    color: "#2A2A2A"
  },
})

module.exports = NewListModal
